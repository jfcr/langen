<?php
declare(strict_types=1);


namespace Interfaces;


interface Language
{

    /**
     *
     * Get own alphabet
     * @return string
     */
    public function getAlphabet():string;


    /**
     * Get foo letters
     * @return string
     */
    public function getFooLetters():string;

    /**
     * Get bar letters
     * @return string
     */
    public function getBarLetters():string;

    /**
     * Determine if is a preposition
     * @param $word
     * @return bool
     */
    public function isPreposition(string $word):bool;


    /**
     * Determine if is a verb or verb subjunctive
     * @param string $word
     * @return int
     */
    public function isVerb(string $word):int;


    /**
     * Get a paragraph sorted by own alphabet
     * @param string $paragraph
     * @return array
     */
    public function getParagraphSorted(string $paragraph):array;

    /**
     * Convert a word into a number
     * @param string $word
     * @return int
     */
    public function convertToNumber(string $word):int;

    /**
     * Determine if is a pretty number
     * @param string $word
     * @return bool
     */
    public function isPrettyNumber(string $word):bool;

}