<?php


namespace Traits;


use Classes\Sort;

trait Sortable
{
    /**
     * @inheritDoc
     */
    public function getParagraphSorted(string $paragraph): array
    {
        $sort   =   new Sort($this->alphabet);
        return $sort->getParagraphSorted($paragraph);
    }

}