<?php


namespace Traits;


trait Numerable
{

    function convertToNumber(string $word):int{

        $chars          =   str_split($word);
        $number         =   0;

        foreach($chars as $index=>$char){
            $tmp = pow($this->base,$index);
            $number += ($tmp * $this->inverted[$char]);
        }

        return (int)$number;
    }

    public function isPrettyNumber(string $word):bool {
        $number =$this->convertToNumber($word);

        if($number<81827) {
            return false;
        }

        if($number % 3 !== 0) {
            return false;
        }

        return true;
    }
}