<?php
declare(strict_types=1);

namespace Traits;
use Classes\Sort;


trait Wordable
{
    /**
     * Extract words correctly
     * @param string $paragraph
     * @return array
     */
    public function to_words(string $paragraph):array{
        return Sort::to_words($paragraph);
    }

}