<?php
declare(strict_types=1);
/**
 * Googlon implementation for Language interface
 * @author Fernando Castillo <desarrollo@freengers.com>
 *
 */

namespace Classes;


use Interfaces\Language;
use Traits\Wordable;
use Traits\Sortable;
use Traits\Numerable;

final class GooglonLanguage implements Language
{
    use Wordable,Sortable,Numerable;

    private $alphabet   =   'sxocqnmwpfyheljrdgui';
    private $fooLetters =   'udxsmpf';
    private $barLetters =   '';
    private $base       =   20; //Thank's sonar but it is used for numerable trait
    private $inverted   =   [];

    function __construct()
    {
        $array              =   str_split($this->alphabet);
        $fooLetters         =   str_split($this->fooLetters);
        $this->barLetters   =   implode('',array_diff($array,$fooLetters));

        //For number calculation
        $base_indexes       =   str_split($this->alphabet);
        $this->inverted     =   array_flip($base_indexes);

        unset($array,$fooLetters,$base_indexes);
    }



    /**
     * @inheritDoc
     */
    public function getAlphabet(): string
    {
        return  $this->alphabet;
    }

    /**
     * @inheritDoc
     */
    public function getFooLetters(): string
    {
        return $this->fooLetters;
    }

    /**
     * @inheritDoc
     */
    public function getBarLetters(): string
    {
        return $this->barLetters;
    }

    /**
     * @inheritDoc
     */
    public function isPreposition(string $word): bool
    {
        /*
         *  Prepositions
         *  The linguists have discovered that in the Googlon language, the prepositions are the words of exactly 6 letters
         *  which end in a foo letter and do not contain the letter u.
         */
        $return = true;
        $last_character = substr($word, -1);

        if(strpos($word,'u')!==false) {
            $return=false;
        }

        if(strlen($word)!==6){
            $return=false;
        }

        if(strpos($this->fooLetters,$last_character)===false){
            $return=false;
        }

        return $return;
    }


    /**
     * @inheritDoc
     */
    public function isVerb(string $word): int
    {
        /*
         *
         *  Another interesting fact discovered by linguists is that, in the Googlon language, verbs are words of 6 letters or
         *  more that end in a bar letter. Furthermore, if a verb starts in a bar letter, then the verb is inflected in its
         *  subjunctive form.
         *
         */

        $result             =   1; //Is a verb
        $last_character     =   substr($word, -1);
        $first_character    =   substr($word, 0, 1);

        if(strlen($word)<6){
            $result=0;
        }

        if(strpos($this->barLetters,$last_character)===false){
            $result=0;
        }

        if ($result===0){
            return $result;
        }


        if(strpos($this->barLetters,$first_character)!==false)   {
            $result=2;
        }  //Is subjunctive

        return $result;
    }


}