<?php
declare(strict_types=1);
/**
 * Order words based in a pattern given
 * @author Fernando Castillo <desarrollo@freengers.com>
 *
 */


namespace Classes;


final class Sort
{
    private $pattern    =   '';
    private $sortBase   =   [];

    /**
     * Sort constructor.
     * @param string $pattern
     */
    function __construct(string $pattern)
    {
        $this->pattern      =   $pattern;
        $this->sortBase     =   $this->calculateSortArray();

    }

    /**
     * Splits a paragraph into words array
     * @param string $paragraph
     * @return array
     */
    public static function to_words(string $paragraph):array{
        $tmp =preg_replace('~[\r\n]+~', ' ', $paragraph );
        return explode(" ",$tmp);
    }


    /**
     * Build an array to be used in sort function
     * @param int $padding
     * @return array
     */

    private function calculateSortArray(int $padding=3): array
    {
        $array_padded =[];
        foreach (array_flip(str_split($this->pattern)) as $key=>$index){
            $array_padded[$key]=str_pad((string)$index,$padding,"0",STR_PAD_LEFT);
        }
        return $array_padded;
    }


    /**
     * Array which will be used for sorting (for testing proposes)
     * @return array
     */
    public function getSortBase(): array
    {
        return $this->sortBase;
    }



    /**
     * Get a paragraph sorted by own pattern
     * @param $paragraph string
     * @return array
     */
    public function getParagraphSorted(string $paragraph): array
    {

        $words      =   self::to_words($paragraph);
        $new_array  =   [];

        foreach($words as $word){
            $tmp    =   str_split($word);
            $tmp2   =   [];
            foreach($tmp as $char){
                $tmp2[] = $this->sortBase[$char];
            }

            $new_array['a'.implode('',$tmp2)]=$word;
        }

        ksort($new_array,SORT_NATURAL);
        return array_values($new_array);
    }

}