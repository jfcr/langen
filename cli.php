<?php
include('vendor/autoload.php');
error_reporting(0);

use Classes\GooglonLanguage;
use tests\Unit\CasesClass;

$red        = "\e[31m";
$white      = "\e[0m";
$green      = "\e[0;32m";
$yellow     = "\e[1;33m";
$blue       = "\e[0;34m";


system('clear');
fwrite(STDOUT, "{$green}==== Welcome to Googlon Language! ===={$white}\n\n");

fwrite(STDOUT, "Please type absolute path of file which contains the paragraph to be analyzed: \n");
fwrite(STDOUT, "{$yellow}a, b, c, d, e) for default paragraphs: {$white}");
$input =  trim(fgets(STDIN));


$language       =   new GooglonLanguage;
$case           =   new CasesClass;
$paragraph      =   '';

$input          = strtolower($input);
$paragraph      = $case->{"paragraph_{$input}"};

if(is_null($paragraph)) {
    $paragraph = file_get_contents($input);
}

if($paragraph===false)  {
    die("{$red}**** File or option invalid *****\n\n{$white}Program finished.\n");
}




$array          =   $language->to_words($paragraph);
$prepositions   =   0;
$verbs          =   0;
$subjunctive    =   0;
$prettyNumbers  =   0;
$vocabulary     =   '';


/*===  Analise functions ===*/
foreach($array as $word){
    if($language->isPreposition($word)){
        $prepositions++;
    }

    $result = $language->isVerb($word);
    if($result > 0){
        $verbs++;
        if($result>1) {
            $subjunctive++;
        }
    }

    if($language->isPrettyNumber($word)) {
        $prettyNumbers++;
    }
}

$vocabulary         =   wordwrap(implode(", ",$language->getParagraphSorted($paragraph)),90);

/*=== End analise functions ===*/




/* ==============
 * RESULTS
 * ==============
 */
fwrite(STDOUT, "\nInput: \n{$paragraph}\n");
fwrite(STDOUT, "{$blue}\n==== Results ====\n");


/* ==============
 * PREPOSITIONS
 * ==============
 */
fwrite(STDOUT, "1) There are {$green}{$prepositions}{$blue} prepositions in the text.\n");


/* ==============
 * VERBS
 * ==============
 */
fwrite(STDOUT, "2) There are {$green}{$verbs}{$blue} verbs in the text.\n");
fwrite(STDOUT, "3) There are {$green}{$subjunctive}{$blue} subjunctive verbs in the text.\n");


/* ==============
 * VOCABULARY
 * ==============
 */
fwrite(STDOUT, "4) Vocabulary list: {$green}{$vocabulary}{$blue}\n");

/* ==============
 * PRETTY NUMBERS
 * ==============
 */
fwrite(STDOUT, "5) There are {$green}{$prettyNumbers}{$blue} distinct pretty numbers in the text. \n");



fwrite(STDOUT, "{$white}==== End of results ====\n\n");




