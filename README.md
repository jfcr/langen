# The Googlon Language Challenge
[![Sonarcloud Status](https://sonarcloud.io/api/project_badges/measure?project=Fernando-Castillo_langen&metric=alert_status)](https://sonarcloud.io/dashboard?id=Fernando-Castillo_langen)


### About the challenge

Please read pdf requirements inside repository.
### Solution
##### Installation

This solution is built in PHP7+. Future language implementations can be done quickly creating a new instance of language interface.

```sh
$ cd langen
$ composer install
```

Run the 28 tests.

```sh
$ ./vendor/bin/phpunit
```

##### Interactive cli


```sh
$ php ./cli.php
```

##### CI/CD with Jenkins and Docker

This project could be tested in Jenkins. The Jenkins file will try to build and execute a dummy docker instance in order to get the project installed. 

Once successfully installed all unit test are executed. 

Finally the dummy docker image will be erased.

##### Requirements for Jenkins

Be sure docker and docker-compose are installed into Jenkins server.
```sh
$ docker --version
Docker version 18.09.6, build 481bc77

$ docker-compose --version
docker-compose version 1.24.0, build 0aa59064
```

If Jenkins can not create docker image give permission  jenkins-user to execute docker:

```sh
$ usermod -a -G docker jenkins
```

##### SonarCloud / SonarQube Integration
A new stage for Jenkins was added in order to work with SonarCube or SonarCloud.

Check my [SonarCloud]  site  for this project.

##### Current Jenkins stages
![Current Jenkins stages](current_jenkins_stages.png)

### Fernando castillo <desarrollo@freengers.com>

| [Personal-Site] 
| [Linkedin] 
| [Resume] 


   [Personal-Site]: <http://freengers.com/>
   [Linkedin]: <https://www.linkedin.com/in/jose-fernando-castillo-rosas-a72542117/>
   [Resume]: <https://www.visualcv.com/fernando-castillo-english/pdf/>
   [SonarCloud]: <https://sonarcloud.io/dashboard?id=Fernando-Castillo_langen>
