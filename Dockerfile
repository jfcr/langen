FROM php:7.2-cli

RUN pecl install pecl install xdebug-2.7.2 \
    && docker-php-ext-enable xdebug

#Avoid some console messages
RUN apt-get update && apt-get install -y --no-install-recommends apt-utils

#Installing composer
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
RUN php -r "if (hash_file('sha384', 'composer-setup.php') === '48e3236262b34d30969dca3c37281b3b4bbe3221bda826ac6a9a62d6444cdb0dcd0615698a5cbe587c3f0fe57a54d8f5') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
RUN php composer-setup.php --install-dir=/usr/local/bin --filename=composer
RUN php -r "unlink('composer-setup.php');"

#Allow to run composer as root
#RUN export COMPOSER_ALLOW_SUPERUSER=1
ENV COMPOSER_ALLOW_SUPERUSER 1

#Installing git
RUN apt-get update && apt-get install -y git

#Copy code base
COPY . ./

#Installing dependencies
RUN composer install


#TESTING
#RUN ./vendor/bin/phpunit