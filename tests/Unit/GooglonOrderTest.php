<?php
declare(strict_types=1);

namespace tests\Unit;

use PHPUnit\Framework\TestCase;
use Classes\GooglonLanguage;
use Traits\Wordable;

final class GooglonOrderTest extends TestCase
{
    use Cases,Wordable;

    function testOrderTextA(){

        $language       =   new GooglonLanguage;
        $expected       =   $this->to_words($this->paragraph_sorted_a);
        $actual         =   $language->getParagraphSorted($this->paragraph_a);

        $this->assertEquals($expected,$actual);

    }

    function testOrderTextB(){

        $language       =   new GooglonLanguage;
        $expected       =   $this->to_words($this->paragraph_sorted_b);
        $actual         =   $language->getParagraphSorted($this->paragraph_b);

        $this->assertEquals($expected,$actual);

    }

    function testOrderTextC(){

        $language       =   new GooglonLanguage;
        $expected       =   $this->to_words($this->paragraph_sorted_c);
        $actual         =   $language->getParagraphSorted($this->paragraph_c);

        $this->assertEquals($expected,$actual);

    }

    function testOrderTextD(){

        $language       =   new GooglonLanguage;
        $expected       =   $this->to_words($this->paragraph_sorted_d);
        $actual         =   $language->getParagraphSorted($this->paragraph_d);

        $this->assertEquals($expected,$actual);

    }

    function testOrderTextE(){

        $language       =   new GooglonLanguage;
        $expected       =   $this->to_words($this->paragraph_sorted_e);
        $actual         =   $language->getParagraphSorted($this->paragraph_e);

        $this->assertEquals($expected,$actual);

    }

}