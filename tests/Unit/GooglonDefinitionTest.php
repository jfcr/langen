<?php
declare(strict_types=1);

namespace tests\Unit;

use PHPUnit\Framework\TestCase;
use Classes\GooglonLanguage;
use Classes\Sort;

final class GooglonDefinitionTest extends TestCase{

    protected $alphabet = 'sxocqnmwpfyheljrdgui';

    function testAlphabet(){

        $language = new GooglonLanguage;
        $this->assertEquals($this->alphabet,$language->getAlphabet());
    }

    function testFooLetters(){
        $fooLetters = 'udxsmpf';
        $language = new GooglonLanguage;
        $this->assertEquals($fooLetters,$language->getFooLetters());
    }

    function testBarLetters(){
        $barLetters = 'ocqnwyheljrgi';
        $language = new GooglonLanguage;
        $this->assertEquals($barLetters,$language->getBarLetters());
    }

    function testAlphabetSortBase(){

        $sort       = new Sort($this->alphabet,[]);
        $expected = [];
        for($i=0;$i<20;$i++){
            $expected[]=str_pad((string)$i,3,"0",STR_PAD_LEFT);
        }
        $this->assertEquals(array_values($expected),array_values($sort->getSortBase()));
    }
}