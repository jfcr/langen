<?php

declare(strict_types=1);

namespace tests\Unit;

use PHPUnit\Framework\TestCase;
use Classes\Sort;

final class SortTestTest extends TestCase
{
    use Cases;

    function testGooglonAlphabet(){
        $alphabet = 'sxocqnmwpfyheljrdgui';

        $sort           =   new Sort($alphabet);
        $expected       =   Sort::to_words($this->paragraph_sorted_a);
        $actual         =   $sort->getParagraphSorted($this->paragraph_a);

        $this->assertEquals($expected,$actual);
    }

    /**
     * https://www.geeksforgeeks.org/sort-the-array-of-strings-according-to-alphabetical-order-defined-by-another-string/
     */
    function test1Geeks4Geeks(){
        $alphabet = 'fguecbdavwyxzhijklmnopqrst';

        $sort           =   new Sort($alphabet);
        $expected       =   ['for','geeksforgeeks', 'best', 'is', 'learning', 'place', 'the'];
        $paragraph      =   implode(" ",['geeksforgeeks', 'is', 'the', 'best', 'place', 'for', 'learning']);
        $actual         =   $sort->getParagraphSorted($paragraph);


        $this->assertEquals($expected,$actual);
    }

    /**
     * https://www.geeksforgeeks.org/sort-the-array-of-strings-according-to-alphabetical-order-defined-by-another-string/
     */
    function test2Geeks4Geeks(){
        $alphabet = 'avdfghiwyxzjkecbmnopqrstul';

        $sort           =   new Sort($alphabet);
        $expected       =   ['consists','colours','of','rainbow'];
        $paragraph      =   implode(" ",['rainbow', 'consists', 'of', 'colours']);
        $actual         =   $sort->getParagraphSorted($paragraph);


        $this->assertEquals($expected,$actual);
    }
}