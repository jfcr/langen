<?php
declare(strict_types=1);

namespace tests\Unit;

use PHPUnit\Framework\TestCase;
use Classes\GooglonLanguage;


final class GooglonNumbersTest extends TestCase{
    use Cases;

    function testGxjrc(){
        $language       =   new GooglonLanguage;
        $word           =   'gxjrc';

        $this->assertEquals(605637,$language->convertToNumber($word));
    }


    function testNumberTextA(){

        $language       =   new GooglonLanguage;
        $words          =   $language->to_words($this->paragraph_a);

        $count=0;
        foreach($words as $word){
            if($language->isPrettyNumber($word)){
                $count++;
            }
        }

        $this->assertEquals(22,$count);

    }

    function testNumberTextB(){

        $language       =   new GooglonLanguage;
        $words          =   $language->to_words($this->paragraph_b);

        $count=0;
        foreach($words as $word){
            if($language->isPrettyNumber($word)) {
                $count++;
            }
        }

        $this->assertEquals(21,$count);

    }

    function testNumberTextC(){

        $language       =   new GooglonLanguage;
        $words          =   $language->to_words($this->paragraph_c);

        $count=0;
        foreach($words as $word){
            if($language->isPrettyNumber($word)) {
                $count++;
            }
        }

        $this->assertEquals(27,$count);

    }

    function testNumberTextD(){

        $language       =   new GooglonLanguage;
        $words          =   $language->to_words($this->paragraph_d);

        $count=0;
        foreach($words as $word){
            if($language->isPrettyNumber($word)) {
                $count++;
            }
        }

        $this->assertEquals(27,$count);

    }

    function testNumberTextE(){

        $language       =   new GooglonLanguage;
        $words          =   $language->to_words($this->paragraph_e);

        $count=0;
        foreach($words as $word){
            if($language->isPrettyNumber($word)) {
                $count++;
            }
        }

        $this->assertEquals(26,$count);

    }

}