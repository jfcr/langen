<?php
declare(strict_types=1);

namespace tests\Unit;

use PHPUnit\Framework\TestCase;
use Classes\GooglonLanguage;
use Traits\Wordable;

define('VERB','verb');
define('SUBJUNCTIVE','subj verbs');

final class GooglonVerbsTest extends TestCase
{
    use Cases,Wordable;

    function testIsVerbTextA(){

        $language       = new GooglonLanguage;

        $array          =   $this->to_words($this->paragraph_a);
        $verbs          =   0;
        $subjunctive    =   0;
        $found          =   [];

        foreach($array as $word){
            $result = $language->isVerb($word);
            if($result > 0){
                $found[] = $word;
                $verbs++;
                if($result>1) {
                    $subjunctive++;
                }
            }
        }
        $this->assertEquals(36,$verbs,VERB);
        $this->assertEquals(25,$subjunctive,SUBJUNCTIVE);

    }


    function testIsVerbTextB(){

        $language       = new GooglonLanguage;

        $array          =   $this->to_words($this->paragraph_b);
        $verbs          =   0;
        $subjunctive    =   0;
        $found          =   [];

        foreach($array as $word){
            $result = $language->isVerb($word);
            if($result > 0){
                $found[] = $word;
                $verbs++;

                if($result>1) {
                    $subjunctive++;
                }
            }
        }

        $this->assertEquals(46,$verbs,VERB);
        $this->assertEquals(26,$subjunctive,SUBJUNCTIVE);

    }

    function testIsVerbTextC(){

        $language       = new GooglonLanguage;

        $array          =   $this->to_words($this->paragraph_c);
        $verbs          =   0;
        $subjunctive    =   0;
        $found          =   [];

        foreach($array as $word){
            $result = $language->isVerb($word);
            if($result > 0){
                $found[] = $word;
                $verbs++;
                if($result>1) {
                    $subjunctive++;
                }
            }
        }
        $this->assertEquals(37,$verbs,VERB);
        $this->assertEquals(22,$subjunctive,SUBJUNCTIVE);

    }

    function testIsVerbTextD(){

        $language       = new GooglonLanguage;

        $array          =   $this->to_words($this->paragraph_d);
        $verbs          =   0;
        $subjunctive    =   0;
        $found          =   [];

        foreach($array as $word){
            $result = $language->isVerb($word);
            if($result > 0){
                $found[] = $word;
                $verbs++;
                if($result>1) {
                    $subjunctive++;
                }
            }
        }
        $this->assertEquals(31,$verbs,VERB);
        $this->assertEquals(19,$subjunctive,SUBJUNCTIVE);

    }

    function testIsVerbTextE(){

        $language       = new GooglonLanguage;

        $array          =   $this->to_words($this->paragraph_e);
        $verbs          =   0;
        $subjunctive    =   0;
        $found          =   [];

        foreach($array as $word){
            $result = $language->isVerb($word);
            if($result > 0){
                $found[] = $word;
                $verbs++;
                if($result>1) {
                    $subjunctive++;
                }
            }
        }

        $this->assertEquals(32,$verbs,VERB);
        $this->assertEquals(19,$subjunctive,SUBJUNCTIVE);

    }


}