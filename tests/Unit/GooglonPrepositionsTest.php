<?php
declare(strict_types=1);

namespace tests\Unit;

use PHPUnit\Framework\TestCase;
use Classes\GooglonLanguage;
use Traits\Wordable;


final class GooglonPrepositionsTest extends TestCase
{
    use Cases,Wordable;

    function testIsPrepositionTextA(){

        $language       = new GooglonLanguage;

        $array          =   $this->to_words($this->paragraph_a);
        $count          =   0;
        $found          =   [];

        foreach($array as $word){
            if($language->isPreposition($word)){
                $found[] = $word;
                $count++;
            }
        }
        $this->assertEquals(3,$count);


    }


    function testIsPrepositionTextB(){

        $language       = new GooglonLanguage;

        $array          =   $this->to_words($this->paragraph_b);
        $count  =   0;
        $found  =   [];
        foreach($array as $word){
            if($language->isPreposition($word)){
                $found[] = $word;
                $count++;
            }
        }
        $this->assertEquals(3,$count);


    }



    function testIsPrepositionTextC(){

        $language       = new GooglonLanguage;

        $array          =   $this->to_words($this->paragraph_c);
        $count  =   0;
        $found  =   [];
        foreach($array as $word){
            if($language->isPreposition($word)){
                $found[] = $word;
                $count++;
            }
        }
        $this->assertEquals(2,$count);


    }


    function testIsPrepositionTextD(){

        $language       = new GooglonLanguage;

        $array          =   $this->to_words($this->paragraph_d);
        $count  =   0;
        $found  =   [];
        foreach($array as $word){
            if($language->isPreposition($word)){
                $found[] = $word;
                $count++;
            }
        }
        $this->assertEquals(3,$count);


    }


    function testIsPrepositionTextE(){

        $language       = new GooglonLanguage;

        $array          =   $this->to_words($this->paragraph_e);
        $count  =   0;
        $found  =   [];
        foreach($array as $word){
            if($language->isPreposition($word)){
                $found[] = $word;
                $count++;
            }
        }
        $this->assertEquals(1,$count);


    }

}